import pygame
import time
clock=pygame.time.Clock()

#Initialize
pygame.init()
w=1366
h=768
icon=pygame.image.load("icon.png")
GD=pygame.display.set_mode((w,h),pygame.FULLSCREEN)
pygame.display.set_caption("Snakes & Ladder")
pygame.display.set_icon(icon)
pygame.display.update()
intbg=pygame.image.load("intropic.png")
intbg2=pygame.image.load("intropic2.png")
intbg3=pygame.image.load("intropic3.png")
intbg4=pygame.image.load("intropic4.png")
intbg5=pygame.image.load("intropic5.png")
pygame.mixer.music.load("music.wav")

     
def intro():
    pygame.mixer.music.play(-1)
    time=pygame.time.get_ticks()
    while pygame.time.get_ticks()-time<2500:
        GD.blit(intbg,(0,0))
        pygame.display.update()
    while True:
        time=pygame.time.get_ticks()
        while pygame.time.get_ticks()-time<500:    
            GD.blit(intbg2,(0,0))
            pygame.display.update()
        time=pygame.time.get_ticks()
        while pygame.time.get_ticks()-time<500:
            GD.blit(intbg3,(0,0))
            pygame.display.update()
        time=pygame.time.get_ticks()
        while pygame.time.get_ticks()-time<500:
            GD.blit(intbg4,(0,0))
            pygame.display.update()
        time=pygame.time.get_ticks()
        while pygame.time.get_ticks()-time<500:
            GD.blit(intbg5,(0,0))
            pygame.display.update()
            
        for event in pygame.event.get():
            if event.type==pygame.KEYDOWN:
                return
        pygame.display.update()
        
intro()
pygame.quit()
